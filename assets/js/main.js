$(document).ready(function () {
  $(".banner-slide").slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: true,
  });
  $(".testimonial-slide").slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  });
  $(".partner-slide").slick({
    infinite: true,
    variableWidth: true,
    slidesToScroll: 1,
    arrows: false,
  });
  $(".header-icon").click(function () {
    $(".header-nav").css("width", "100%");
  });
  $(".close-menu").click(function () {
    $(".header-nav").css("width", "0%");
  });
});
